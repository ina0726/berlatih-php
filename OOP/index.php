<?php
require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal ("shaun");
echo "Nama Binatang : $sheep->name <br>";
echo "Jumlah Binatang : $sheep->legs <br>";
echo "Apakah ini barang : $sheep->cold_blooded <br> <br>";

$kodok = new Frog ("buduk");
echo "Kodok : $kodok->name <br>";
echo "Hewan Berkaki : $kodok->katak <br>";
echo "Menurunkan sifat kaki 2 : $kodok->cold_blooded <br>";
echo $kodok->jump();
echo "<br><br>";
$sungokong = new Ape ("Kera Sakti");
echo "Sungokong : $sungokong->name <br>";
echo "Hewan Berkaki : $sungokong->kera <br>";
echo $sungokong->yell();
?>

